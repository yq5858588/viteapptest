import {createApp} from 'vue'
import App from './App.vue'
const app = createApp(App);
import './assets/style.css'
// import DataV, { setClassNamePrefix } from '@dataview/datav-vue3';
import VXETable from 'vxe-table'
import 'vxe-table/lib/style.css'
import router from './router/routerindex'
import * as echarts from 'echarts'
// vue3 给原型上挂载属性
app.config.globalProperties.$echarts = echarts;

import {
    getToken,
    setToken,
    removeToken,
} from './utils/auth.js'
app.config.globalProperties.$getToken = getToken;
app.config.globalProperties.$setToken = setToken;
app.config.globalProperties.$removeToken = removeToken;

function useTable(app) {
    app.use(VXETable)
    // 给 vue 实例挂载内部对象，例如：
    // app.config.globalProperties.$XModal = VXETable.modal
    // app.config.globalProperties.$XPrint = VXETable.print
    // app.config.globalProperties.$XSaveFile = VXETable.saveFile
    // app.config.globalProperties.$XReadFile = VXETable.readFile
}
app.use(useTable)

//引入layui相关组件
import Layui from '@layui/layui-vue'
import '@layui/layui-vue/lib/index.css'
app.use(Layui)

//引入ElementPlus相关组件
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
app.use(ElementPlus, {size: 'small', locale: zhCn, zIndex: 3000});
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}

//引入ViewUIPlus相关组件
import ViewUIPlus from 'view-ui-plus'
import 'view-ui-plus/dist/styles/viewuiplus.css'
app.use(ViewUIPlus, {
    transfer: true,
    size: 'small',
    capture: false,
    select: {
        arrow: 'md-arrow-dropdown',
        arrowSize: 20
    }
})
app.use(router)
//引入pinia
import { createPinia } from 'pinia' //引入pinia
app.use(createPinia())
//引入pinia

// app.use(DataV, { classNamePrefix: 'dv-' });
app.mount('#app')
