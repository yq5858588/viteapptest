'use strict';

const Controller = require('egg').Controller;

class HomeController extends Controller {
    async index() {
        const { ctx } = this;
        console.log(this);
        ctx.body = '我是首页';
    }

    async login() {
        const { ctx } = this;
        ctx.body = 'ni 这里是登录页面';
    }

    async wel() {
        const { ctx } = this;
        // console.log(  this.config.userConfig.api);

        const res = await ctx.curl('https://api.vc.bilibili.com/dynamic_svr/v1/dynamic_svr/web_homepage?alltype_offset=629878364372019802&video_offset=631261781994504200&article_offset=623641882878920300');
        console.log(JSON.parse(res.data));

        const userlist = await this.ctx.service.user.getAll();

        console.log(userlist);
        // ctx.body = userlist;
        await this.ctx.render('wel', { userlist });
    }
}

module.exports = HomeController;
