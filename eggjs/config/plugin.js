'use strict';

/** @type Egg.EggPlugin */
module.exports = {
    // had enabled by egg
    static: {
        enable: true,
        package: 'egg-view-nunjucks',
    },
    mysql: {
        enable: true,
        package: 'egg-mysql',
    },
    // io: {
    //     enable: true,
    //     package: 'egg-socket.io',
    // }
};


