import Vue from 'vue'
import Vuex from 'vuex'
import state from './state';
import getters from './getters';
import actions from './actions';
import mutations from './mutations';
import user from './modules/user.js';
Vue.use(Vuex)

export default new Vuex.Store({
    state: state,
    getters: getters,
    mutations: mutations,
    actions: actions,
    modules: {
        user
    }
})
