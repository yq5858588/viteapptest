import Vue from 'vue'
import VueRouter from 'vue-router'
import ViewUI from 'view-design';
Vue.use(ViewUI);
Vue.use(VueRouter)
const routes = [
    {
        path: '/',
        name: 'root',
        redirect: "admin",
    },
    {
        path: '/admin',
        name: 'admin',
        component: () => import('@/views/layout/layout.vue'),
        // redirect: "admin_center",
        children: [
            {
                path: '/admin/system',
                name: 'system',
                component: () => import('@/views/system/index.vue'),
                children: [
                    {
                        path: 'user',
                        name: 'user',
                        component: () => import('@/views/system/user/index.vue')
                    },
                    {
                        path: 'role',
                        name: 'role',
                        component: () => import( '@/views/system/role/index.vue')
                    },
                    {
                        path: 'node',
                        name: 'role',
                        component: () => import( '@/views/system/node/index.vue')
                    },
                ]
            },
            {
                path: '/admin/goods',
                name: 'goods',
                component: () => import( '@/views/system/index.vue'),
                children: [
                    {
                        path: 'goodscat',
                        name: 'goodscat',
                        component: () => import('@/views/goods/goodscat/index.vue')
                    },
                    {
                        path: 'goodslist',
                        name: 'goodslist',
                        component: () => import('@/views/goods/goodslist/index.vue')
                    },
                ]
            },
            {
                path: '/admin/center',
                name: 'admin_center',
                component: () => import('@/views/system/center.vue'),
            },
        ]
    },
    {
        path: '/about',
        name: 'about',
        meta: {
            title: "关于",
        }
    },
    {
        path: '/login',
        name: 'login',
        component: () => import(/* webpackChunkName: "about" */ '@/views/public/login.vue'),
    },
    {
        path: '*',
        name: 'about',
        component: () => import(/* webpackChunkName: "about" */ '@/views/public/error.vue'),
    }
]
const router = new VueRouter({
    mode: 'history',
    // base: process.env.BASE_URL,
    base: import.meta.env.BASE_URL,
    routes
})
window.sessionStorage.setItem('userInfo', true);
// window.sessionStorage.clear();
router.beforeEach((to, from, next) => {
    // 我在这里模仿了一个获取用户信息的方法
    let isLogin = window.sessionStorage.getItem('userInfo');
    console.log(to);
    if (isLogin) {
        //如果用户信息存在则往下执行。
        ViewUI.LoadingBar.start();
        next();
    } else {
        //如果用户token不存在则跳转到login页面
        if (to.path === '/login') {
            next()
        } else {
            next('/login')
        }
    }
});
router.afterEach(route => {
    ViewUI.LoadingBar.finish();
});
export default router
