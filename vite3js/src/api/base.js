/**
 * 接口路径配置
 */
const base = {
    goodsList: '/api/getSysUserList',//商品列表
    login: '/api/login',
}
export default base;
