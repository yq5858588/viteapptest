var express = require('express');
var router = require('./router');
var app = express();

app.use('/api', router);

app.listen(8989,()=>{
    console.log(  "http://192.168.0.6:8989/api"  );
});
