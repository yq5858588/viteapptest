import {createRouter, createWebHashHistory} from 'vue-router'
import ViewUIPlus from 'view-ui-plus'
//路由数组
const routes = [
    {
        path: '/',
        redirect: {name: 'admin'},
    },
    {
        path: '/admin',
        name: 'admin',
        // redirect: {name: 'admin/wel1'},
        meta: {isLogin: true,},
        // redirect: { name: 'equipment' },
        // component: () => import('../views/layout/layout_element.vue'),
        component: () => import('../views/layout/layout_layui.vue'),
        // component: () => import('../views/layout/layout_viewui.vue'),
        children: [
            {
                des: 'wel_element',
                name: 'admin/wel_element',
                path: '/admin/wel_element',
                component: () => import('../views/layout/wel_element.vue'),
                // redirect: 'system/sysuser',
            },
            {
                des: 'wel_element',
                name: 'admin/wel_viewui',
                path: '/admin/wel_viewui',
                component: () => import('../views/layout/wel_viewui.vue'),
                // redirect: 'system/sysuser',
            },
            {
                des: 'wel_layui',
                name: 'admin/wel_layui',
                path: '/admin/wel_layui',
                component: () => import('../views/layout/wel_layui.vue'),
                // redirect: 'system/sysuser',
            },
            {
                des: '欢迎页面',
                name: 'admin/welcome',
                path: '/admin/welcome',
                component: () => import('../views/layout/welcome.vue'),
                // redirect: 'system/sysuser',
            },
            {
                des: '百度地图',
                name: '/admin/map/baidu',
                path: '/admin/map/baidu',
                component: () => import('../views/map/baidu.vue'),
                // redirect: 'system/sysuser',
            },
            {
                des: '高德地图',
                name: '/admin/map/gaode',
                path: '/admin/map/gaode',
                component: () => import('../views/map/gaode.vue'),
                // redirect: 'system/sysuser',
            },
            {
                des: '腾讯地图',
                name: '/admin/map/qq',
                path: '/admin/map/qq',
                component: () => import('../views/map/qq.vue'),
                // redirect: 'system/sysuser',
            },
            {
                des: '天地图',
                name: '/admin/map/tiandi',
                path: '/admin/map/tiandi',
                component: () => import('../views/map/tiandi.vue'),
                // redirect: 'system/sysuser',
            },
            {
                des: '系统设置',
                path: '/admin/system',
                component: () => import('../views/system/index.vue'),
                // redirect: 'system/sysuser',
                children: [
                    {
                        des: '系统用户',
                        path: '/admin/system/sysuser',
                        component: () => import('../views/system/sysuser/index.vue')
                    }, {
                        des: '系统菜单',
                        path: '/admin/system/sysmenu',
                        component: () => import('../views/system/sysmenu/index.vue')
                    }, {
                        des: '系统角色',
                        path: '/admin/system/sysrole',
                        component: () => import('../views/system/sysrole/index.vue')
                    }]
            },
            {
                des: '采购设置',
                path: '/admin/goods',
                component: () => import('../views/goods/index.vue'),
                // redirect: 'system/sysuser',
                children: [
                    {
                        des: '商品管理',
                        path: '/admin/goods/goodslist',
                        component: () => import('../views/goods/goodslist/index.vue')
                    }, {
                        des: '商品类别管理',
                        path: '/admin/caigou/sysmenu',
                        component: () => import('../views/goods/goodslist/index.vue')
                    }, {
                        des: '系统角色',
                        path: '/admin/caigou/sysrole',
                        component: () => import('../views/goods/goodslist/index.vue')
                    }]
            }
        ]
    },
    {
        des: '登陆',
        path: '/login',
        component: () => import('../views/public/login.vue')
    },
    {
        des: '注册',
        path: '/register',
        component: () => import('../views/public/login.vue')
    }
]
//路由对象
const router = createRouter({
    // history: createWebHistory(process.env.BASE_URL),
    history:createWebHashHistory(),
    routes //上面的路由数组
})
router.beforeEach((to, from, next) => {
    ViewUIPlus.LoadingBar.start();
    next();
});

router.afterEach(route => {
    ViewUIPlus.LoadingBar.finish();
});
//导出路由对象，在main.js中引用
export default router
