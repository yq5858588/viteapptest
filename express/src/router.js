const express = require('express');

const router = express.Router();

const sqlFn = require('./mysql');

const jwt = require('jsonwebtoken');
const config = require('./config');

router.get('/', function (req, res) {
    res.send('用户首页');
});

router.post('/login', (req, res) => {

    let { username, password } = req.body;
    let sql = "select * from sysuser where username=? and password=?";
    sqlFn(sql, [username, password], data => {
        if (data.length > 0) {
            res.send({
                status: 200,
                msg: "查询成功",
                data: data,
                total: data.length
            })
        } else {
            res.send({
                status: 500,
                msg: '暂无数据'
            });
        }
    })

    res.send('用户首页');
});

// router.get('/:id', function (req, res) {
//     res.send(`${req.params.id} 用户信息`);
// });

router.get('/getSysUserList', (req, res) => {
    const page = req.query.page || 1;
    const sql = "select * from stdchk_item where id ";
    sqlFn(sql, null, data => {
        if (data.length > 0) {
            res.send({
                status: 200,
                msg: "查询成功",
                data: data,
                total: data.length
            })
        } else {
            res.send({
                status: 500,
                msg: '暂无数据'
            });
        }
    })
});

//导出该路由
module.exports = router;
