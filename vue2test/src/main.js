import Vue from 'vue'
import App from './App.vue'
import router from './router/routerindex'
import store from './store/storeindex'
import busEvent from './utils/busEvent.js'


import XEUtils from 'xe-utils'
import VXETable from 'vxe-table'
import 'vxe-table/lib/style.css'

Vue.use(VXETable)


import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css';
Vue.use(ViewUI, {
    transfer: true,
    size: 'small',
    capture: false,
    select: {
        arrow: 'md-arrow-dropdown',
        arrowSize: 20
    }
});

Vue.prototype.$busEvent=busEvent;
Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
