import request from "../../utils/request.js";
export default {
    getSysuserList(params) {
        return request.post('/api/getSysUserList', params);
    },
    getSysroleList(params) {
        return request.get('/api/getSysUserList', params);
    },
    getSysmenuList(params) {
        return request.get('/api/getSysUserList', params);
    }
}
