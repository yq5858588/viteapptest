'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
  router.get('/wel', controller.home.wel);
  router.get('/login', controller.home.login);
  router.get('/news', controller.news.index);
  router.get('/newsinfo', controller.news.info);
};
