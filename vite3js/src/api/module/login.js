import request from "../../utils/request.js";
export default {
    login(params) {
        return request.post('/web-interface/nav', params);
    },

    reg(params) {
        return request.get('/api/getSysUserList', params);
    }
}
