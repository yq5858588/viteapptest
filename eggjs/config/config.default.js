/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
    /**
     * built-in config
     * @type {Egg.EggAppConfig}
     **/
    const config = exports = {};

    // use for cookie sign key, should change to your own and keep security
    config.keys = appInfo.name + '_sdfg';

    // add your middleware config here
    config.middleware = [];

    config.session = {
        key: 'EGG_SESS',
        maxAge: 24 * 3600 * 1000, // 1 天
        httpOnly: true,
        encrypt: true,
    };
    config.view = {
        // root: [
        //   path.join(appInfo.baseDir, 'app/view'),
        //   path.join(appInfo.baseDir, 'path/to/another'),
        // ].join(','),
        defaultViewEngine: 'nunjucks',
        defaultExtension: '.html',
        mapping: {
            '.html': 'nunjucks',
        },
    };
    // add your user config here
    const userConfig = {
        myAppName: 'egg',
        api: 'sdfgsdgsdfgsdfg'
    };

    return {
        ...config,
        userConfig,
    };
};
