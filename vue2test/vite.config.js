import {createVuePlugin} from "vite-plugin-vue2";
import {defineConfig} from "vite";
// const BASE_URL =import.meta.env.BASE_URL === 'production '?'/h5admin':'/'
export default defineConfig({
    lintOnSave: false,
    server: {
        host: "127.0.0.1",
    },
    // baseUrl:BASE_URL,
    plugins: [createVuePlugin({
        vueTemplateOptions: {},
    })],
    resolve: {
        extensions: [".vue", ".mjs", ".js", ".ts", ".jsx", ".tsx", ".json"],
        alias: [
            {
                find: "@",
                replacement: "/src",
            },
        ],
    },
    build: {
        // 设置vite打包大小控制
        chunkSizeWarningLimit: 10000,
    },
});
