import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue';
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import {ElementPlusResolver, LayuiVueResolver} from 'unplugin-vue-components/resolvers'
// https://vitejs.dev/config/
export default defineConfig({
    lintOnSave: false,
    // ...
    define: {
        'process.env': {}
    },
    plugins: [
        vue(),
        // ...
        AutoImport({
            resolvers: [ElementPlusResolver(), LayuiVueResolver()],
        }),
        Components({
            resolvers: [
                ElementPlusResolver(),
                LayuiVueResolver(),
            ],
        }),
    ],
    resolve: {
        extensions: [".vue", ".mjs", ".js", ".ts", ".jsx", ".tsx", ".json"],
        alias: [
            {
                find: "@",
                replacement: "/src",
            },
        ],
    },
    server: {
        host: "127.0.0.1",
        hmr: true,//配置修改静态魔板页面热加载
        open: false,//自动打开浏览器
        // proxy: {// 跨域代理
        //     '/api': {
        //         // target: 'http://' + env.VUE_APP_BASE_API,
        //         target: 'https://api.bilibili.com/', //目标url
        //         changeOrigin: true,//支持跨域
        //         rewrite: (path) => path.replace(/^\/api/, ''),//重写路径,替换/api
        //     },
        //     // 代理 WebSocket 或 socket
        //     // '/socket.io': {
        //     //   target: 'ws://localhost:3000',
        //     //   ws: true
        //     //  }
        // },
    },
})
