const getters = {
    appNameWidthVersion: (state) => {
        return state.appName + 'V2.0'
    }
}
export default getters
