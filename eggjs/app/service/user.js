// app/service/user.js
const Service = require('egg').Service;

class UserService extends Service {
    async getAll() {
        const user = await this.app.mysql.get('sys_user', { id: 1 });
        console.log(  user  );
        return { user };
    }
    async find(uid) {
        const user = await this.ctx.db.query(
          'select * from user where uid = ?',
          uid,
        );
        return user;
    }
}

module.exports = UserService;
