var mysql = require('mysql');
var client = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password: 'root',
    database: 'boottest'
});

// client.connect();
//
// client.query('SELECT 1 + 1 AS solution', function (error, results, fields) {
//     if (error) throw error;
//     console.log('The solution is: ', results[0].solution);
// });

function sqlFun(sql,arr,callback){
    client.query(sql,arr, function (error, results, fields) {
        if (error) {
            console.log(  '数据库语句错误'  );
            return;
        }
        callback(results);
    });
}
module.exports=sqlFun;


