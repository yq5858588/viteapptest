/**
 * 所有请求的方法
 */
import request from '../utils/request.js';
import system from './module/system.js'
import login from './module/login.js'

// console.log(   import.meta.env   );
const api = {
    ...login,
    ...system,
}
export default api;
